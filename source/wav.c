#include "wav.h"

u64 littleEndianToUnsignedInt(char *le, size_t sz)
{
    u64 ui = 0;

    int i;
    for(i = 0; i < sz; i++)
    {
        int j;
        u64 power = 1;
        for(j = 0; j < i*2; j++)
            power *= 16;
        ui += (u64)(le[i]) * power;
    }

    return ui;
}

int extractAudioInfo(char *wavB, Wavestruct *wavS)
{
    wavS->audioFormat = littleEndianToUnsignedInt(&(wavB[20]), 2);
    wavS->audioSize = littleEndianToUnsignedInt(&(wavB[40]), 4);
    wavS->channelCount = littleEndianToUnsignedInt(&(wavB[22]), 2);
    wavS->sampleRate = littleEndianToUnsignedInt(&(wavB[24]), 4);
    wavS->bitrate = (littleEndianToUnsignedInt(&(wavB[28]), 4)*8);
    wavS->blockAlign = littleEndianToUnsignedInt(&(wavB[32]), 2);
    wavS->bitsPerSample = littleEndianToUnsignedInt(&(wavB[34]), 2);
    wavS->audioData = linearAlloc(sizeof(char) * wavS->audioSize);
    if(wavS->audioData == NULL)
        return -1;
    memcpy(wavS->audioData, &(wavB[44]), wavS->audioSize);
    return 0;
}

void printAudioInfo(Wavestruct *wavS)
{
    printf("Audio format: %s (%llu)\n", (wavS->audioFormat == 1) ? "PCM" : "UNKNOWN", wavS->audioFormat);
    printf("Audio data size: %llu\n", wavS->audioSize);
    printf("Number of channels: %llu\n", wavS->channelCount);
    printf("Sample rate: %llu\n", wavS->sampleRate);
    printf("Bitrate: %llu\n", wavS->bitrate);
    printf("Block align: %llu\n", wavS->blockAlign);
    printf("Bits per sample: %llu\n", wavS->bitsPerSample);
}

int openAudioFile(const char *filename, long *filesize, char **wavFile)
{
    FILE *fp;
    fp = fopen(filename, "rb");
    if(!fp)
    {
        printf("COULD NOT OPEN FILE\n");
        svcSleepThread(3000000000);
        gfxExit();
        return EXIT_FAILURE;
    }

    fseek(fp, 0L, SEEK_END);
    *filesize = ftell(fp);
    printf("Filesize: %ld\n", *filesize);
    rewind(fp);

    *wavFile = calloc(sizeof(char), *filesize);
    if(*wavFile == NULL)
    {
        printf("COULD NOT ALLOCATE DATA\n");
        svcSleepThread(3000000000);
        fclose(fp);
        fp = NULL;
        gfxExit();
        return EXIT_FAILURE;
    }

    if(*filesize != fread(*wavFile, sizeof(char), *filesize, fp))
    {
        printf("COULD NOT WRITE FILE CONTENTS TO BUFFER\n");
        svcSleepThread(3000000000);
        free(*wavFile);
        *wavFile = NULL;
        fclose(fp);
        fp = NULL;
        gfxExit();
        return EXIT_FAILURE;
    }

    fclose(fp);
    fp = NULL;

    return EXIT_SUCCESS;
}