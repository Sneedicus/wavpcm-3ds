#include "wav.h"

static const char *filename = "sdmc:/3ds/WAVPCM-3DS/sounds/SONICJMP.wav";
long filesize = 0;
char *wavFile;

int main(int argc, char **argv)
{
    gfxInitDefault();
    consoleInit(GFX_TOP, NULL);

    printf("%s\n", filename);
    printf("Press A to play.\n");
    printf("Press Start to exit.\n");

    u32 oDown = 0;

    int res = openAudioFile(filename, &filesize, &wavFile);
    if(res)
        return res;

    char riff[5] = "";
    memcpy(riff, wavFile, 4);
    printf("|%s|\n", riff);

    char wave[5] = "";
    memcpy(wave, &(wavFile[8]), 4);
    printf("|%s|\n", wave);

    char frmt[5] = "";
    memcpy(frmt, &(wavFile[12]), 4);
    printf("|%s|\n", frmt);

    char data[5] = "";
    memcpy(data, &(wavFile[36]), 4);
    printf("|%s|\n", data);

    Wavestruct waveInfo;
    if(extractAudioInfo(wavFile, &waveInfo) == -1)
    {
        printf("COULD NOT ALLOCATE AUDIO DATA\n");
        svcSleepThread(3000000000);
        free(wavFile);
        wavFile = NULL;
        gfxExit();
        return EXIT_FAILURE;
    }

    free(wavFile);
    wavFile = NULL;

    ndspInit();
    ndspSetOutputMode(NDSP_OUTPUT_MONO);
	ndspChnSetInterp(0, NDSP_INTERP_LINEAR);
	ndspChnSetRate(0, waveInfo.sampleRate);
	ndspChnSetFormat(0, NDSP_FORMAT_MONO_PCM8);

    float mix[12];
	memset(mix, 0, sizeof(mix));
	mix[0] = 1.0;
	mix[1] = 1.0;
	ndspChnSetMix(0, mix);

    ndspWaveBuf waveBuf;
    waveBuf.data_vaddr = waveInfo.audioData;
    waveBuf.nsamples = (waveInfo.audioSize)/(waveInfo.bitsPerSample/8);

    printAudioInfo(&waveInfo);
    
    // main loop
    while(aptMainLoop())
    {
        hidScanInput();

        u32 kDown = hidKeysDown();
        if(kDown & KEY_START) break;

        if((kDown != oDown) && (kDown & KEY_A))
        {
            // play audio
            DSP_FlushDataCache(waveBuf.data_vaddr, waveBuf.nsamples);
            ndspChnWaveBufAdd(0, &waveBuf);
        }

        oDown = kDown;

        gfxFlushBuffers();
        gfxSwapBuffers();
        gspWaitForVBlank();
    }

    waveBuf.data_vaddr = NULL;
    ndspExit();
    linearFree(waveInfo.audioData);
    waveInfo.audioData = NULL;
    gfxExit();
    return EXIT_SUCCESS;
}