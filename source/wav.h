#ifndef WAV_H
#define WAV_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <3ds.h>

/* STRUCTS */

typedef struct
{
    u8 *audioData;
    u64 audioFormat;
    u64 audioSize;
    u64 channelCount;
    u64 sampleRate;
    u64 bitrate;
    u64 blockAlign;
    u64 bitsPerSample;
} Wavestruct;

/* FUNCTIONS */

u64 littleEndianToUnsignedInt(char *le, size_t sz);
int extractAudioInfo(char *wavB, Wavestruct *wavS);
void printAudioInfo(Wavestruct *wavS);
int openAudioFile(const char *filename, long *filesize, char **wavFile);

#endif /*#ifndef WAV_H*/